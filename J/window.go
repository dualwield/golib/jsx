package J

import (
	"gitlab.com/dualwield/golib/jsx/C"
	"syscall/js"
)

func Window() js.Value {
	return js.Global().Get(C.Window)
}
