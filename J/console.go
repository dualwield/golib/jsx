package J

import (
	"gitlab.com/dualwield/golib/jsx/C"
	"syscall/js"
)

func Log(x string, xs ...string) {
	js.Global().Get(C.Console).Call(C.Log, Cons(x, xs...))
}

func Logjs(x js.Value) {
	js.Global().Get(C.Console).Call(C.Log, x)
}
