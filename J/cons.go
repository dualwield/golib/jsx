package J

/*
This might not stay in, but it's handy so for now it stays.
*/

// Cons concats a string, and a space between each additional string
func Cons(x string, xs ...string) (z string) {
	if len(xs) == 0 {
		return x
	}
	z = x
	for _, i := range xs {
		z += " "
		z += i
	}

	return
}
