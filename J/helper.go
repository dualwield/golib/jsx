package J

import (
	fmt "gitlab.com/cathalgarvey/fmtless"
	"gitlab.com/dualwield/golib/jsx/C"
	"syscall/js"
)

func Style(e js.Value) js.Value {
	return e.Get(C.Style)
}

func Px(s int) string {
	const t = `%dpx`
	return fmt.Sprintf(t, s)
}
