package J

import (
	"gitlab.com/dualwield/golib/jsx/C"
	"syscall/js"
)

func Document() js.Value {
	return js.Global().Get(C.Document)
}
