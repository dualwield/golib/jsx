package C

const Style = "style"

const (
	Absolute = "absolute"
	Border   = "border"
	Bottom   = "bottom"
	Height   = "height"
	Left     = "left"
	Position = "position"
	Right    = "right"
	Solid    = "solid"
	Top      = "top"
	Width    = "width"
)

const (
	Red = "red"
)
