package C

const (
	LocalStorage   = "localStorage"
	SessionStorage = "sessionStorage"
)

const (
	Clear   = "clear"
	GetItem = "getItem"
	SetItem = "setItem"
)
